from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


from attendees.models import AccountVO


def updateAccountVO(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)

    if is_active:
        created = AccountVO.objects.update_or_create({})

    else:
        AccountVO.objects.filter(email=email).delete()


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
while True:
    try:
        credentials = PlainCredentials("username", "password")
        parameters = ConnectionParameters("hostname", 5672, "/", credentials)
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host="localhost"))
        channel = connection.channel()

        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
#     create a blocking connection with the parameters
#     open a channel
#
#     declare a fanout exchange named "account_info"
        channel.exchange_declare(exchange="account_info", exchange_type="fanout")
#      declare a randomly-named queue
        result = channel.queue_declare(queue="account_info_q", exclusive=True)
        account_info_q = result.method.queue
#      get the queue name of the randomly-named queue
#
#      bind the queue to the "account_info" exchange
        channel.queue_bind(exchange="account_info", queue=account_info_q)
#      do a basic_consume for the queue name that calls
#          function above
        def callback_account_info(ch, method, properties, body):
            print(" [x] %r" % body.decode())

        print('[*] Waiting for logs. To exit press CTRL+C')
        channel.basic_consume(
            queue=account_info_q, on_message_callback=callback_account_info, auto_ack=True)
    #      tell the channel to start consuming
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)



# def main():
# #    connection = pika.BlockingConnection(
# #       pika.ConnectionParameters(host='localhost'))
#  #   channel = connection.channel()

#     channel.exchange_declare(exchange='logs', exchange_type='fanout')

#     result = channel.queue_declare(queue='', exclusive=True)
#     queue_name = result.method.queue

#     channel.queue_bind(exchange='logs', queue=queue_name)

#     def callback(ch, method, properties, body):
#         print(" [x] %r" % body.decode())

#     print(' [*] Waiting for logs. To exit press CTRL+C')
#     channel.basic_consume(
#         queue=queue_name, on_message_callback=callback, auto_ack=True)



# if __name__ == '__main__':
#     try:
#         main()
#     except KeyboardInterrupt:
#         print('Interrupted')
#         try:
#             sys.exit(0)
#         except SystemExit:
#             os._exit(0)
