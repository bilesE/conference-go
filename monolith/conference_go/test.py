# class Counter:
#     count = 0

#     def get_count(self):
#         return self.count

#     def increment(self):
#         self.count += 1


# class ConfigurableCounter(Counter):
#     def __init__(self, change_by):
#         self.change_by = change_by

#     def increment(self):
#         self.count += self.change_by

# counter1 = Counter()
# counter1.increment()
# counter1.increment()
# counter1.increment()

# counter2 = Counter()
# counter2.increment()
# counter2.increment()

# counter3 = Counter()
# counter3.increment()

# print(counter3.get_count())
